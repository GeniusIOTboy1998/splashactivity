package com.example.splashactivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_splash);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        RelativeLayout layoutSplash = (RelativeLayout) findViewById(R.id.activity_splash);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.1f,0.1f);
        alphaAnimation.setDuration(3000);

        layoutSplash.startAnimation(alphaAnimation);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            // the animation start
            @Override
            public void onAnimationStart(Animation animation) {

            }

            // the animation end
            @Override
            public void onAnimationEnd(Animation animation) {
                // 设置页面跳转的逻辑
                Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(intent);
            }

            // repeat
            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        // 休眠一段时间后，启动MainActivity

    }
}
